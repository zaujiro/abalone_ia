/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import fileController.FileController;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.regex.Pattern;
import javax.swing.JLabel;
import javax.swing.*;
import tablero.Ficha;
import tablero.Tablero;

/**
 * Johan Córdoba 201556020 Laura Romero 201556275 Kevin Ramrez 201556203
 */
public class GUI extends JFrame implements MouseListener {

    /**
     * controlador de archivos *
     */
    private FileController fileController;

    /**
     * Cantidad maxima de fichas que se pueden marcar *
     */
    private int maxFichas = 0;

    /**
     * Indica si el juego se ha iniciado*
     */
    private boolean iniciado = false;

    /**
     * Tablero de juego *
     */
    private Tablero tablero;

    /**
     * Fichas seleccionadas *
     */
    private ArrayList<int[]> fichasSeleccionadas = new ArrayList<int[]>();

    private ArrayList<Ficha> posicionesPosibles = new ArrayList<Ficha>();

    /* Guarda las coordenadas posibles en una posición x,y de un turno **/
    ArrayList<int[]> movimientosPosibles = new ArrayList<int[]>();

    /**
     * Coordenada origen de la jugada
     */
    int[] coordenadaOrigen = {-1, -1};
    /**
     * Coordenada destino de la jugada
     */
    int[] coordenadaDestino = {-1, -1};

    /**
     * Constructor de la clase
     */
    public GUI() {

        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        /**
         * Tablero inicial por defecto
         */
        String tableroString = "2 2 2 2 2 -1 -1 -1 -1\n"
                + "2 2 2 2 2 2 -1 -1 -1\n"
                + "0 0 2 2 2 0 0 -1 -1\n"
                + "0 0 0 0 0 0 0 0 -1\n"
                + "0 0 0 0 0 0 0 0 0\n"
                + "0 0 0 0 0 0 0 0 -1\n"
                + "0 0 1 1 1 0 0 -1 -1\n"
                + "1 1 1 1 1 1 -1 -1 -1\n"
                + "1 1 1 1 1 -1 -1 -1 -1 ";

        /**
         * Creamos el tablero con el string de tablero inicial *
         */
        tablero = new Tablero(tableroString);

        /**
         * Dinujamos el tablero inicial *
         */
        dibujarTablero(tablero);

        /**
         * Imagen inicial *
         */
        labelTurnoActual.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ready.png")));
        labelTuTurno.setText("");
//        dibujarTablero();
    }

    public void dibujarTablero(Tablero tablero) {
        /**
         * Limpiamos el panel antes de dibujar el tablero *
         */
        limpiarPanel();

        /**
         * Inicial de fichas en cada fila *
         */
        int fichasFila = 5;

        /**
         * Ubicación inicial de las fichas
         */
        int xi = 130;
        int yi = 40;

        /**
         * Variable para el control del incremento en x
         */
        int x = 0;

        /**
         * Variable de control para decrementar las fichas *
         */
        boolean mitad = false;

        for (int f = 0; f < 9; f++) {

            for (int k = 0; k < fichasFila; k++) {
                /**
                 * Figura que se pone en el panel para mostrar el abalone *
                 */
                JLabel label = new JLabel();
                label.setBounds(xi + x, yi, 60, 50);
                label.setIcon(new javax.swing.ImageIcon(getClass().getResource(tablero.getMatriz()[f][k].getImg())));

                panelPrincipal.add(label);

                x += 40;

                int fila = f;
                int columna = k;

                /**
                 * Activamos los eventos del mouse
                 */
                label.addMouseListener(new java.awt.event.MouseAdapter() {
                    /**
                     * Evento cuando se da clic en el label
                     *
                     * @param evt
                     */
                    public void mouseClicked(java.awt.event.MouseEvent evt) {

                        System.out.println("(" + fila + "," + columna + ")");

                        /**
                         * Comprobamos que se haya iniciado el juego y aún se
                         * permita seleccionar fichas
                         */
                        if (iniciado && maxFichas > 0) {

                            if (tablero.getMatriz()[fila][columna].isSeleccionable()) {

                                /**
                                 * Si la ficha es seleccionable, comprobamos los
                                 * posibles movimientos que se pueden realizar
                                 * en esa posición y los resaltamos
                                 */
                                System.out.println("Movimientos permitidos para " + "(" + fila + "," + columna + ")");

                                /**
                                 * Si aún no hay movimientos posibles es porque
                                 * no se ha seleccionado una ficha en el tablero
                                 * para jugar
                                 */
                                if (movimientosPosibles.size() == 0) {
                                    movimientosPermitidos(fila, columna);
                                    /**
                                     * Actualizamos la coordenada origen de la
                                     * jugada*
                                     */
                                    coordenadaOrigen[0] = fila;
                                    coordenadaOrigen[1] = columna;

                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource(tablero.getFichaSeleccionada().getImg())));
//                                    maxFichas--;
                                } else {
                                    /**
                                     * Si hay movimientos posibles, comprobamos
                                     * que la ficha seleccionada esté en los
                                     * posibles movimientos, si está entonces
                                     * podemos empujar el resto de las canicas
                                     * de lo contrario se espera una coordenada
                                     * valida
                                     */

                                    if (movimientoValido(fila, columna)) {

                                        /**
                                         * Actualizamos la coordenada origen de
                                         * la jugada*
                                         */
                                        coordenadaDestino[0] = fila;
                                        coordenadaDestino[1] = columna;
                                        /**
                                         * Realizamos la jugada y empujamos las
                                         * canicas
                                         */

                                        realizarJugada(fila, columna);
                                    }

                                }

                            }

                        }
                    }

                    /**
                     * Evento cuando se pasa el mouse por el label
                     *
                     * @param evt
                     */
                    public void mouseEntered(java.awt.event.MouseEvent evt) {

                        /**
                         * Obtenemos la ficha de la posición x,y *
                         */
                        String imgFicha = tablero.getMatriz()[fila][columna].getImg();
                        /**
                         * Separamos la cadena por "." *
                         */
                        String parts[] = imgFicha.split(Pattern.quote("."));
                        /**
                         * Construimos el nombre de la imagen difusa*
                         */
                        String imagenDifusion = parts[0] + "_difusion." + parts[1];

                        /**
                         * Si la ficha no es seleccionable es porque es una
                         * posición donde podemos poner la ficha,cambiamos la
                         * imagen por la imagen difusa
                         */
                        if (!tablero.getMatriz()[fila][columna].isSeleccionable() && tablero.getMatriz()[fila][columna].getFichaN() == 0) {
                            label.setIcon(new javax.swing.ImageIcon(getClass().getResource(imagenDifusion)));
                        }

                    }

                    /**
                     * Evento cuando se sale del are del label
                     *
                     * @param evt
                     */
                    public void mouseExited(java.awt.event.MouseEvent evt) {
                        /**
                         * Obtenemos la ficha de la posición x,y *
                         */
                        String imgFicha = tablero.getMatriz()[fila][columna].getImg();

                        /**
                         * Si la ficha no es seleccionable es porque es una
                         * posición donde podemos poner la ficha,cambiamos la
                         * imagen por la imagen difusa
                         */
                        if (!tablero.getMatriz()[fila][columna].isSeleccionable()) {
                            label.setIcon(new javax.swing.ImageIcon(getClass().getResource(imgFicha)));
                        }

                    }
                });

            }

            x = 0;

            if (fichasFila > 8) {
                mitad = true;
                xi = 50;
                yi = 184;
            }

            if (!mitad) {
                xi -= 20;
                yi += 36;
                fichasFila++;
            } else {

                xi += 20;
                yi += 36;
                fichasFila--;
            }

        }

        panelPrincipal.paint(panelPrincipal.getGraphics());
    }

    /**
     * Remueve todos los elementos del panel para dibujar otro
     */
    private void limpiarPanel() {
        panelPrincipal.removeAll();
        panelPrincipal.repaint();
    }

    /**
     * Solicitamos la ruta del archivo donde se encuentra la getMatriz del grafo
     */
    private void openFile() {
        /**
         * Pedimos el archivo que contiene la ruta mediante una ventana
         */
        JFileChooser fileChooser = new JFileChooser();
        int selection = fileChooser.showOpenDialog(panelPrincipal);

        /**
         * Si se seleccionó se crea el archivo con la clase auxiliar
         * FileCOntroller
         */
        if (selection == JFileChooser.APPROVE_OPTION) {
            fileController = new FileController(fileChooser.getSelectedFile());
            /**
             * Creamos el tableroTable que estará en el juego
             * fileController.getFileContent()
             */
            tablero = new Tablero(fileController.getFileContent());
            reiniciarJuego();
            dibujarTablero(tablero);
        }
    }

    private void ponerImgBorde(int fila, int columna) {

        /**
         * Obtenemos la ficha de la posición x,y *
         */
        String imgFicha = tablero.getMatriz()[fila][columna].getImg();
        /**
         * Separamos la cadena por "." *
         */
        String parts[] = imgFicha.split(Pattern.quote("."));
        /**
         * Construimos el nombre de la imagen difusa*
         */
        String imagenDifusion = parts[0] + "_difusion." + parts[1];

    }

    private void jugar() {
        /**
         * Comprobar el turno actual para permitir la selección solo de las
         * fichas de ese jugador
         */

        System.out.println("Turno actual = " + tablero.getJugadorEnTurno().getNumero());
        if (tablero.getJugadorEnTurno().getNumero() == 1) {
            /**
             * Si el turno actual es del jugador 1, deshabilitamos la selección
             * de fichas del jugador 2
             */

            tablero.getFichaJugador1().setSeleccionable(true);
            tablero.getFichaJugador2().setSeleccionable(false);

        } else if (tablero.getJugadorEnTurno().getNumero() == 2) {
            /**
             * Si el turno actual es del jugador 2, deshabilitamos la selección
             * de fichas del jugador 1
             */
            tablero.getFichaJugador1().setSeleccionable(false);
            tablero.getFichaJugador2().setSeleccionable(true);

        }
    }

    private void realizarJugada(int fila, int columna) {

        System.out.println("Realizar Jugada: Origen [" + coordenadaOrigen[0] + "][" + coordenadaOrigen[1] + "] Destino [" + fila
                + "][" + columna + "]");
        /**
         * Copia del tablero para realizar la jugada *
         */
        Tablero tablero = new Tablero(this.tablero);
        tablero.getMatriz()[fila][columna] = tablero.getJugadorEnTurno().getFicha();
        tablero.getMatriz()[coordenadaOrigen[0]][coordenadaOrigen[1]] = tablero.getFichaLibre();
        
        
        dibujarTablero(tablero);

    }

    /**
     * Comprueba si una coordenada que recibe por parámetro se encuentra en la
     * lista de posibles movimientos
     *
     * @param fila
     * @param columna
     * @return true si está en la lista de movimientos
     */
    private boolean movimientoValido(int fila, int columna) {

        boolean valido = false;

        /**
         * Leemos todoso los posibles movimientos *
         */
        for (int i = 0; i < movimientosPosibles.size(); i++) {
            /**
             * Comparamos cada coordenada en la lista con la fila y columna que
             * se recibe por parametro, si la encuentra cambia el estado de la
             * variable valido a true y termina el ciclo
             */
            if (movimientosPosibles.get(i)[0] == fila && movimientosPosibles.get(i)[1] == columna) {
                valido = true;
                break;
            }
        }

        return valido;
    }

    /**
     * Dada una coordenada dice hacia donde se puede mover la ficha
     *
     * @param x
     * @param y
     */
    private void movimientosPermitidos(int x, int y) {

        boolean encontrado = false;

        /**
         * Creamos todas las posibles coordendas de una coordenada x,y
         */
        int[] coodernada1 = {x, y - 1};
        int[] coodernada2 = {x, y + 1};
        int[] coodernada3 = {x - 1, y};
        int[] coodernada4 = {x - 1, y + 1};
        int[] coodernada5 = {x + 1, y - 1};
        int[] coodernada6 = {x + 1, y};

        /**
         * Agregamos todas las coordenadas calculadas a una lista para comprobar
         * cada una en un ciclo
         */
        movimientosPosibles.add(coodernada1);
        movimientosPosibles.add(coodernada2);
        movimientosPosibles.add(coodernada3);
        movimientosPosibles.add(coodernada4);
        movimientosPosibles.add(coodernada5);
        movimientosPosibles.add(coodernada6);

        /**
         * Validamos las posibles coordenadas para mover la ficha y empujar las
         * canicas
         */
        validarCoordenadasPosibles(movimientosPosibles);

        for (int i = 0; i < movimientosPosibles.size(); i++) {

            System.out.println("[" + movimientosPosibles.get(i)[0] + "," + movimientosPosibles.get(i)[1] + "]");

        }

    }

    /**
     * Valida si la coordenada es valida, si no es valida la quita de la lista
     * de coordenadas posibles
     *
     * @param coordenadas_posibles
     */
    private void validarCoordenadasPosibles(ArrayList<int[]> coordenadas_posibles) {
        for (int i = 0; i < coordenadas_posibles.size(); i++) {
            /**
             * Si la coordenada se sale de los limites, no la tomamos como una
             * posible coordenada
             */
            if (coordenadas_posibles.get(i)[0] > 8 || coordenadas_posibles.get(i)[1] < 1) {
                coordenadas_posibles.remove(i);
            } else {
                /**
                 * Comprobamos si la ficha en la coordenada de la matriz es una
                 * ficha limite también la removemos de las coordenadas posibles
                 */
                if (tablero.getMatriz()[coordenadas_posibles.get(i)[0]][coordenadas_posibles.get(i)[1]].isLimite()) {
                    coordenadas_posibles.remove(i);
                }
            }
        }
    }

    /**
     * Inica todas las variables necesarias para el juego
     */
    private void iniciarJuego() {

        /**
         * Si no se ha iniciado ponemos el máximo de fichas que se pueden
         * seleccionar
         */
        if (!iniciado) {
            maxFichas = 3;
        }

        /**
         * Inician las fichas negras, jugador 1
         */
        tablero.setJugadorEnTurno(1);

        /**
         * Cargamos la imagen en el label del turno para que sepan quien inicia
         * el juego *
         */
        labelTurnoActual.setIcon(new javax.swing.ImageIcon(getClass().getResource(tablero.getJugadorEnTurno().getFicha().getImg())));
        labelTuTurno.setText("es tu turno!");
        /**
         * Ponemos en juego en iniciado para permitir que se marquen las fichas
         * que se pueden seleccionar *
         */
        iniciado = true;

        jugar();
    }

    /**
     * Reinicia las variables de inicio del juego
     */
    private void reiniciarJuego() {
        labelTurnoActual.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ready.png")));
        labelTuTurno.setText("");
        iniciado = false;
        maxFichas = 3;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelPrincipal = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnCargarAmbiente = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        listModoJuego = new javax.swing.JComboBox<>();
        labelJugador = new javax.swing.JLabel();
        labelTurnoActual = new javax.swing.JLabel();
        labelTuTurno = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelPrincipal.setBackground(new java.awt.Color(102, 102, 102));

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 582, Short.MAX_VALUE)
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Baskerville Old Face", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Abalone");

        btnCargarAmbiente.setText("Cargar ambiente");
        btnCargarAmbiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarAmbienteActionPerformed(evt);
            }
        });

        jLabel3.setText("Como quieres jugar ? ");

        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });

        listModoJuego.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Jugador vs Jugador", "Jugador vs Maquina", "Maquina vs Maquina" }));
        listModoJuego.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listModoJuegoActionPerformed(evt);
            }
        });

        labelJugador.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        labelJugador.setText("Jugardor ");

        labelTuTurno.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        labelTuTurno.setText("es tu tuno!");

        jButton1.setText("Iniciar Juego");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 456, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(56, 56, 56))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(98, 98, 98))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(182, 182, 182)
                                .addComponent(labelTurnoActual, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(166, 166, 166)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelTuTurno)
                                    .addComponent(labelJugador))))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(65, 65, 65)
                                .addComponent(btnCargarAmbiente)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(301, 301, 301)
                                        .addComponent(jLabel2))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(70, 70, 70)
                                        .addComponent(listModoJuego, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(154, 154, 154)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(99, 99, 99)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCargarAmbiente)
                            .addComponent(listModoJuego, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addComponent(labelJugador)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelTurnoActual, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelTuTurno)
                .addGap(30, 30, 30)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel2MouseClicked

    private void btnCargarAmbienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarAmbienteActionPerformed
        // TODO add your handling code here:
        openFile();
    }//GEN-LAST:event_btnCargarAmbienteActionPerformed

    private void listModoJuegoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listModoJuegoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_listModoJuegoActionPerformed

    /**
     * Cuando se presiona el botón de iniciar juego preparamos todo el tablero y
     * las fichas
     *
     * @param evt
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        iniciarJuego();

    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUI().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCargarAmbiente;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel labelJugador;
    private javax.swing.JLabel labelTuTurno;
    private javax.swing.JLabel labelTurnoActual;
    private javax.swing.JComboBox<String> listModoJuego;
    private javax.swing.JPanel panelPrincipal;
    // End of variables declaration//GEN-END:variables

    @Override
    public void mouseClicked(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

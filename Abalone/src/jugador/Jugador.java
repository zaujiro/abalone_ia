package jugador;

import tablero.Ficha;

public class Jugador {

    private int numero;
    private Ficha ficha;

    public Jugador(int numero, Ficha ficha) {
        this.numero = numero;
        this.ficha = ficha;
    }

    public int getNumero() {
        return numero;
    }

    public Ficha getFicha() {
        return ficha;
    }

}

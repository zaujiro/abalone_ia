package tablero;

/**
 * Clase tablero.Ficha, contiene los atributos y métodos asociados a una ficha
 * del abalone
 */
public class Ficha {

    int fichaN;
    String img;
    boolean seleccionable;
    boolean limite;

    /**
     * Constructor de la clase ficha
     * @param fichaN número de la ficha
     * @param img string del nombre de la imagen de la ficha
     * @param seleccionable booleano si es seleccionable
     * @param limite  booleano si es un limite del tablero
     */
    public Ficha(int fichaN, String img, boolean seleccionable, boolean limite) {
        this.fichaN = fichaN;
        this.img = img;
        this.seleccionable = seleccionable;
        this.limite = limite;
    }

    /**
     * Obtiene el número que identifica la ficha
     * @return 
     */
    public int getFichaN() {
        return fichaN;
    }

    /**
     * Retorna el string que corresponde al nombre de la imagen de la ficha
     * @return 
     */
    public String getImg() {
        return img;
    }

    /**
     * Indica si la ficha es seleccionable
     * @return 
     */
    public boolean isSeleccionable() {
        return seleccionable;
    }

    /**
     * Retorna si el valor de la ficha es un limite del tablero
     * @return 
     */
    public boolean isLimite() {
        return limite;
    }

    /**
     * Cambia el estado de una ficha para permitir o no la selección de la misma 
     * @param seleccionable 
     */
    public void setSeleccionable(boolean seleccionable) {
        this.seleccionable = seleccionable;
    }
    
    

}

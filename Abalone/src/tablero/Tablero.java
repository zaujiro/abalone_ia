package tablero;

import jugador.Jugador;

/**
 * Clase que controla todos los moviemientos del tablero
 */
public class Tablero {

    /**
     * tablero.Tablero de fichas
     *
     */
    private Ficha[][] matriz;
    /**
     * Filas de la matriz
     *
     */
    private String[] filas;
    /**
     * Columnas de la matriz
     *
     */
    private String[] columnas;

    /**
     * Casilla limite de la matriz
     *
     */
    private Ficha fichaLimite = new Ficha(-1, "/img/vacio.png", false, true);
    /**
     * Casilla libre en la matriz
     *
     */
    private Ficha fichaLibre = new Ficha(0, "/img/negra.png", false, false);

    /**
     * Ficha seleccionada
     */
    private Ficha fichaSeleccionada = new Ficha(0, "/img/verde.png", false, false);

    /**
     * tablero.Ficha negra del tablero *
     */
    private Ficha fichaJugador1 = new Ficha(1, "/img/azul.png", true, false);
    /**
     * tablero.Ficha blanca del tablero *
     */
    private Ficha fichaJugador2 = new Ficha(2, "/img/blanca.png", true, false);

    /**
     * Jugadores en el tablero
     */
    private Jugador jugador1 = new Jugador(1, fichaJugador1);
    private Jugador jugador2 = new Jugador(2, fichaJugador2);
    private Jugador jugadorEnTurno;

    /**
     * Constructor de la clase tablero.Tablero
     *
     * @param tablero
     */
    public Tablero(String tablero) {

        /**
         * leemos cada fila de la mátriz
         */
        filas = tablero.split("\n");

        /**
         * Tomamos el número de filas y como es una matriz cuadrada debe ser el
         * mismo número de columnas
         */
        this.matriz = new Ficha[filas.length][filas.length];

        /**
         * recorremos la getNumeroFilas
         */
        for (int f = 0; f < filas.length; f++) {

            columnas = filas[f].split(" ");
            /**
             * recorremos la columna de cada fila
             */
            for (int k = 0; k < filas.length; k++) {

                /**
                 * Comenzamos a llenar la getMatriz con los valores que se
                 * cargan el archivo plano
                 */
                matriz[f][k] = objetoTablero(Integer.parseInt(columnas[k]));
            }
        }

        mostrarTablero();
    }

    /**
     * Constructor para los hijos de tablero
     *
     * @param tablero
     */
    public Tablero(Tablero tablero) {
        /**
         * Nuevo espacio en memoria para la matriz que vamos a modificar
         */
        this.matriz = new Ficha[tablero.getNumeroFilas()][tablero.getNumeroColmnas()];
        this.jugador1 = tablero.getJugador1();
        this.jugador2 = tablero.getJugador2();
        this.jugadorEnTurno = tablero.getJugadorEnTurno();

        /**
         * Llenamos la matriz con los valores de la matriz que vamos a copiar
         */
        for (int i = 0; i < tablero.getNumeroFilas(); i++) {
            for (int j = 0; j < tablero.getNumeroColmnas(); j++) {
                matriz[i][j] = tablero.getMatriz()[i][j];
            }

        }

    }

    /**
     * Retorna el tipo ce ficha que se pondrá en el tablero
     *
     * @param fichaN el número de la ficha del jugador
     * @return la ficha correspondiente al tablero
     */
    private Ficha objetoTablero(int fichaN) {

        Ficha ficha = fichaLimite;

        if (fichaN == 0) {
            ficha = fichaLibre;
        } else if (fichaN == 1) {
            ficha = fichaJugador1;
        } else if (fichaN == 2) {
            ficha = fichaJugador2;
        }
        return ficha;
    }

    /**
     * Muestra la getMatriz que se llenó con el archivo de texto que se cargó
     */
    public void mostrarTablero() {
        /**
         * *
         * Leemos las getNumeroFilas de la getMatriz
         */
        for (int i = 0; i < filas.length; i++) {
            /**
             * Leemos las getNumeroColmnas de la getMatriz
             */
            for (int j = 0; j < columnas.length; j++) {
                /**
                 * Mostramos el valor en la posición (i,j) *
                 */
                System.out.print(matriz[i][j].getFichaN() + " ");
            }
            /**
             * Salto de linea para cada fila
             */
            System.out.print("\n");
        }
    }

    /**
     * Retorna el jugador en turno
     *
     * @return
     */
    public Jugador getJugadorEnTurno() {
        return jugadorEnTurno;
    }

    /**
     * Retorna el jugador 1
     *
     * @return
     */
    public Jugador getJugador1() {
        return jugador1;
    }

    /**
     * Retorna el juegador 2
     *
     * @return
     */
    public Jugador getJugador2() {
        return jugador2;
    }

    public Ficha getFichaJugador1() {
        return fichaJugador1;
    }

    public Ficha getFichaJugador2() {
        return fichaJugador2;
    }

    public Ficha getFichaLibre() {
        return fichaLibre;
    }

    /**
     * Retorna la ficha que se debe poner para indicar que está seleccionada
     *
     * @return
     */
    public Ficha getFichaSeleccionada() {
        return fichaSeleccionada;
    }

    /**
     * Retorna la cantidad de getNumeroFilas de la getMatriz
     *
     * @return
     */
    public int getNumeroFilas() {
        return filas.length;
    }

    /**
     * retorna la cantidad de getNumeroColmnas de la getMatriz
     *
     * @return
     */
    public int getNumeroColmnas() {
        return columnas.length;
    }

    /**
     * Retorna la matriz del tablero
     *
     * @return
     */
    public Ficha[][] getMatriz() {
        return matriz;
    }

    /**
     * Cambia el jugador en turno
     *
     * @param jugadorEnTurno
     */
    public void setJugadorEnTurno(int jugadorEnTurno) {

        if (jugadorEnTurno == 1) {
            this.jugadorEnTurno = jugador1;
        } else if (jugadorEnTurno == 2) {
            this.jugadorEnTurno = jugador1;
        }
    }

}

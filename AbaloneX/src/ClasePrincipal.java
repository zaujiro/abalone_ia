import javax.swing.*;

/**
 * Clase principal que ejecuta la GUI
 */
public class ClasePrincipal {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Abalone");
        frame.setContentPane(new GUI().getPanelPrincipal());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

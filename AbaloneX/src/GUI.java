import fileController.FileController;
import tablero.IconCellRenderer;
import tablero.Tablero;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Clase que controla la GUI
 */
public class GUI {
    private JComboBox listaOpciones;
    private JButton btnJugar;
    private JButton btnCargarAmbienteButton;
    private JPanel panelPrincipal;
    private JPanel panelTablero;
    private JTable tableroTable;
    private FileController fileController;

    private Tablero tablero;

    public GUI() {


        /**
         * Quitar lineas verticales y horizontales
         */
        tableroTable.setShowHorizontalLines(false);
        tableroTable.setShowVerticalLines(false);

        /**
         * Evento para el botón que carga de la matriz
         */
        btnCargarAmbienteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openFile();
            }
        });

        /**
         * Evento para la tabla
         * Obtenemos las coordenadas la fila y columna seleccionada
         */
        tableroTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int fila = tableroTable.rowAtPoint(e.getPoint());
                int columna = tableroTable.columnAtPoint(e.getPoint());

//                tableroTable.setColumnSelectionAllowed(false);
//                tableroTable.setCellSelectionEnabled(false);

                System.out.println("("+fila+","+columna+")");
            }
        });
    }


    /**
     * Solicitamos la ruta del archivo donde se encuentra la getMatriz del grafo
     */
    private void openFile() {
        /**
         * Pedimos el archivo que contiene la ruta mediante una ventana
         */
        JFileChooser fileChooser = new JFileChooser();
        int selection = fileChooser.showOpenDialog(panelPrincipal);

        /**
         * Si se seleccionó se crea el archivo con la clase auxiliar FileCOntroller
         */
        if (selection == JFileChooser.APPROVE_OPTION) {
            fileController = new FileController(fileChooser.getSelectedFile());

            /**
             * Creamos el tableroTable que estará en el juego
             */

            Tablero tablero = new Tablero(fileController.getFileContent());
            llenarTablero(tablero);


        }
    }

    private void llenarTablero(Tablero tablero) {

        tableroTable.setDefaultRenderer(Object.class, new IconCellRenderer());
        DefaultTableModel modelo = (DefaultTableModel) tableroTable.getModel();

//        tableroTable.setShowHorizontalLines(false);
//        tableroTable.setShowVerticalLines(false);
        /**
         * Indicamos cuantas getNumeroFilas y getNumeroColmnas tendrá la tabla
         */
        modelo.setRowCount(tablero.getNumeroFilas()); // cantidad total de getNumeroFilas
        modelo.setColumnCount(tablero.getNumeroColmnas()); // cantidad total de getNumeroColmnas

        Border border = LineBorder.createGrayLineBorder();
        /**
         * Llenamos cada espacio de la tabla con los datos de la getMatriz
         */
        for (int i = 0; i < tablero.getNumeroFilas(); i++) {
            for (int j = 0; j < tablero.getNumeroColmnas(); j++) {

                ImageIcon img = new ImageIcon(getClass().getResource(tablero.getMatriz()[i][j].getImg()));
                JLabel label = new JLabel("h");
                label.setSize(100,100);
                label.setBorder(border);
                label.setHorizontalTextPosition(JLabel.LEFT);
                label.setVerticalTextPosition(JLabel.BOTTOM);

                tableroTable.setValueAt(label, i, j);


//                tableroTable.setValueAt(tablero.getMatriz()[i][j].getFichaN(), i, j);
                tableroTable.setRowHeight(20);
            }
        }
    }

    /**
     * Retorna el panel principal de la GUI
     *
     * @return
     */
    public JPanel getPanelPrincipal() {
        return panelPrincipal;
    }
}

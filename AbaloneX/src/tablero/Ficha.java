package tablero;

/**
 * Clase tablero.Ficha,
 * contiene los atributos y métodos asociados a una ficha del abalone
 */
public class Ficha {
    int fichaN;
    String img;

    public Ficha(int fichaN, String img){
        this.fichaN = fichaN;
        this.img = img;
    }

    public int getFichaN() {
        return fichaN;
    }

    public String getImg() {
        return img;
    }
}

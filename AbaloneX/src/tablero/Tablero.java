package tablero;

/**
 * Clase que controla todos los moviemientos del tablero
 */
public class Tablero {
    /**
     * tablero.Tablero de fichas
     **/
    private Ficha[][] matriz;
    /**
     * Filas de la matriz
     **/
    private String[] filas;
    /**
     * Columnas de la matriz
     **/
    private String[] columnas;

    /**
     * Casilla limite de la matriz
     **/
    private Ficha fichaLimite = new Ficha(-1, "img/limite.png");
    /**
     * Casilla libre en la matriz
     **/
    private Ficha fichaLibre = new Ficha(0, "img/libre.png");

    /** tablero.Ficha negra del tablero **/
    private Ficha fichaNegra = new Ficha(1, "img/negra.png");
    /** tablero.Ficha blanca del tablero **/
    private Ficha fichaBlanca= new Ficha(2, "img/blanca.png");

    /**
     * Constructor de la clase tablero.Tablero
     *
     * @param tablero
     */
    public Tablero(String tablero) {

        /**
         * leemos cada fila de la mátriz
         */
        filas = tablero.split("\n");

        /**
         * Tomamos el número de filas y como es una matriz cuadrada debe ser el mismo número de columnas
         */
        this.matriz = new Ficha[filas.length][filas.length];

        /**
         * recorremos la getNumeroFilas
         */
        for (int f = 0; f < filas.length; f++) {

            columnas = filas[f].split(" ");
            /**
             * recorremos la columna de cada fila
             */
            for (int k = 0; k < filas.length; k++) {

                /**
                 * Comenzamos a llenar la getMatriz con los valores que se cargan el archivo plano
                 */
                matriz[f][k] = objetoTablero(Integer.parseInt(columnas[k]));
            }
        }

    }

    /**
     * Retorna el tipo ce ficha que se pondrá en el tablero
     *
     * @param fichaN el número de la ficha del jugador
     * @return la ficha correspondiente al tablero
     */
    private Ficha objetoTablero(int fichaN) {

        Ficha ficha = fichaLimite;

        if (fichaN == 0) {
            ficha = fichaLibre;
        } else if (fichaN == 1) {
            ficha = fichaNegra;
        } else if (fichaN == 2) {
            ficha = fichaBlanca;
        }
        return ficha;
    }

    /**
     * Retorna la cantidad de getNumeroFilas de la getMatriz
     *
     * @return
     */
    public int getNumeroFilas() {
        return filas.length;
    }

    /**
     * retorna la cantidad de getNumeroColmnas de la getMatriz
     *
     * @return
     */
    public int getNumeroColmnas() {
        return columnas.length;
    }

    /**
     * Retorna la matriz del tablero
     *
     * @return
     */
    public Ficha[][] getMatriz() {
        return matriz;
    }


}
